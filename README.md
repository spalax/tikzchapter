TikZChapter — A LaTeX package to ease formatting chapter (section, etc.) titles using TikZ
==========================================================================================

[![Sources](https://img.shields.io/badge/sources-tikzchapter-brightgreen.svg)](http://framagit.org/spalax/tikzchapter)
[![CTAN](https://img.shields.io/ctan/v/tikzchapter.svg)](http://ctan.org/pkg/tikzchapter)
[![Documentation](https://img.shields.io/badge/doc-pdf-brightgreen.svg)](http://mirrors.ctan.org/macros/latex/contrib/tikzchapter/tikzchapter.pdf)

> WARNING!
>
> This project is discontinued (before its first release). It was a bad, over-engineered solution to a simple problem that does not deserve its own package.
>
> Or maybe not… I don't know…

TODO Short description

- Version TODO (unreleased)
- TODO Usage and installation instruction are available in the [documentation](http://mirrors.ctan.org/macros/latex/contrib/tikzchapter/tikzchapter.pdf).

Examples
--------

- [Blue arrow](https://framagit.org/spalax/tikzchapter/raw/main/examples/bluearrow.pdf) ([source](https://framagit.org/spalax/tikzchapter/raw/main/examples/bluearrow-code.tex)).
- [Orange](https://framagit.org/spalax/tikzchapter/raw/main/examples/orange.pdf) ([source](https://framagit.org/spalax/tikzchapter/raw/main/examples/orange-code.tex)).
- [Misc1](https://framagit.org/spalax/tikzchapter/raw/main/examples/misc1.pdf) ([source](https://framagit.org/spalax/tikzchapter/raw/main/examples/misc1-code.tex)).

License
-------

*Copyright 2017 Louis Paternault*

This work may be distributed and/or modified under the conditions of the LaTeX
Project Public License, either version 1.3 of this license or (at your option)
any later version.

The latest version of this license is in http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX version
2005/12/01 or later.
