PDFLATEX=pdflatex
LATEX=latex

.PHONY: all
all: tikzchapter.pdf tikzchapter.sty examples

.PHONY: build
build: tikzchapter.pdf tikzchapter.sty README.md LICENSE.txt CHANGELOG.md
	ctanify tikzchapter.ins tikzchapter.dtx tikzchapter.pdf README.md LICENSE.txt CHANGELOG.md

tikzchapter.sty: tikzchapter.ins tikzchapter.dtx
	yes | $(LATEX) tikzchapter.ins

.PHONY: examples
examples: tikzchapter.sty
	cd examples && make examples

tikzchapter.pdf: tikzchapter.dtx tikzchapter.sty
	$(PDFLATEX) tikzchapter.dtx
	makeindex -s gglo.ist -o tikzchapter.gls tikzchapter.glo
	makeindex -s gind.ist -o tikzchapter.ind tikzchapter.idx
	$(PDFLATEX) tikzchapter.dtx
	$(PDFLATEX) tikzchapter.dtx

.PHONY: clean
allclean: clean
	rm -f \
		tikzchapter.pdf \
		tikzchapter.sty

.PHONY: allclean
clean:
	rm -f tikzchapter.aux \
		tikzchapter.glo \
		tikzchapter.gls \
		tikzchapter.idx \
		tikzchapter.ilg \
		tikzchapter.ind \
		tikzchapter.log \
		tikzchapter.out \
		tikzchapter.toc
