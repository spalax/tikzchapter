How to release
==============

1. Check that everything compile:

        make all examples

2. Update version number and release date, at least in: CHANGELOG.md README tikzchapter.dtx tikzchapter.ins

        git grep CURRENT_VERSION

3. Change copyright date, at least in: README tikzchapter.ins tikzchapter.dtx

        git grep CURRENT_COPYRIGHT_YEAR

3. Update changelog

        $EDITOR CHANGELOG.md

4. Mark release in git

        git commit -m "Version VERSION"
        git tag vVERSION

5. Build CTAN package

    make build

6. Upload to CTAN: go to http://ctan.org/pkg/tikzchapter and click on `Upload` (to pre-fill some fields).
